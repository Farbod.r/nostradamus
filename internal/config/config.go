package config

import (
	"strings"
	"time"

	"github.com/go-playground/validator"
	"github.com/spf13/viper"
)

const prefix = "nostenv"

// nolint: gochecknoglobals
var (
	Conf Config
)

func Reset() {
	viper.Reset()
	Conf = Config{}
}

func New(path string) Config {
	v := viper.New()
	v.SetConfigType("yaml")
	v.SetEnvPrefix(prefix)
	v.AddConfigPath(path)
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	v.AutomaticEnv()

	if err := v.MergeInConfig(); err != nil {
		panic(err)
	}

	if err := v.Unmarshal(&Conf); err != nil {
		panic("Cannot unmarshal config file")

	}

	if err := Conf.Validate(); err != nil {
		panic("Cannot validate config file")
	}

	return Conf

}

type (
	Config struct {
		Env    string `mapstructure:"env" validate:"required"`
		Debug  bool   `mapstructure:"debug"`
		App    App    `mapstructure:"app" validate:"required"`
		Logger Logger `mapstructure:"logger" validate:"required"`
		Influx Influx `mapstructure:"influx" validate:"required"`
		Tribe  Tribe  `mapstructure:"tribe" validate:"required"`
	}

	App struct {
		Name                string        `mapstructure:"name" validate:"required"`
		WordAnalysisEnabled bool          `mapstructure:"word-analysis" default:"true"`
		GracefulTimeout     time.Duration `mapstructure:"graceful-timeout" validate:"required"`
		Port                string        `mapstructure:"port" validate:"required"`
	}

	Logger struct {
		Level string `mapstructure:"level" default:"Info"`
	}

	Influx struct {
		URI    string `mapstructure:"uri" validate:"required"`
		Token  string `mapstructure:"token" validate:"required"`
		Bucket string `mapstructure:"bucket" validate:"required"`
		Org    string `mapstructure:"org" validate:"required"`
	}
	Tribe struct {
		ClientID      string `mapstructure:"clientid" validate:"required"`
		ClientSecret  string `mapstructure:"clientsecret" validate:"required"`
		SigningSecret string `mapstructure:"signingsecret" validate:"required"`
	}
)

func (c Config) Validate() error {
	return validator.New().Struct(c)
}
