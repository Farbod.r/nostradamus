package influx

import (
	"context"
	"go.uber.org/zap"
	"time"

	"nostradamus/internal/config"

	"github.com/influxdata/influxdb-client-go/v2"
)

const ConnectionTimeout = 10 * time.Second

func Connect(cfg config.Config, logger *zap.Logger) (influxdb2.Client, error) {

	client := influxdb2.NewClient(cfg.Influx.URI, cfg.Influx.Token)

	{
		ctx, done := context.WithTimeout(context.Background(), ConnectionTimeout)
		defer done()

		pong, err := client.Ping(ctx)

		if err != nil {
			logger.Error("Cannot connect to influxdb", zap.Error(err), zap.Bool("pong", pong))
			return nil, err
		}
	}

	logger.Info("Connected to influx successfully")

	return client, nil
}
