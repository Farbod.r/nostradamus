package store

import (
	"fmt"

	"nostradamus/internal/config"

	"github.com/influxdata/influxdb-client-go/v2"
	"go.uber.org/zap"
)

const (
	wordCollection     = "word.collection"
	sentenceCollection = "sentence.collection"
)

type Store struct {
	SentenceCollection SentenceCollection
	WordCollection     WordCollection
}

// New create a store consists of all collections.
func New(config config.Config, client *influxdb2.Client, log *zap.Logger) *Store {

	return &Store{
		SentenceCollection: SentenceCollection{
			DB:     client,
			Logger: log.Named(fmt.Sprintf("Influxdb.%s", sentenceCollection)),
			Config: config,
		},
		WordCollection: WordCollection{
			DB:     client,
			Logger: log.Named(fmt.Sprintf("Influxdb.%s", wordCollection)),
			Config: config,
		},
	}
}
