package store

import (
	"context"
	"time"

	"nostradamus/internal/config"

	"github.com/influxdata/influxdb-client-go/v2"
	"go.uber.org/zap"
)

type SentenceCollection struct {
	DB     *influxdb2.Client
	Logger *zap.Logger
	Config config.Config
}

func (sc SentenceCollection) Insert(score uint8, sentence string, t time.Time, meta map[string]string) {
	writeApi := (*sc.DB).WriteAPIBlocking(sc.Config.Influx.Org, sc.Config.Influx.Bucket)

	p := influxdb2.NewPoint("sentiment", meta, map[string]interface{}{
		"score": score,
	}, time.Now())

	if err := writeApi.WritePoint(context.Background(), p); err != nil {
		sc.Logger.Error("cannot write sentence point to influxdb", zap.Error(err))
	}

	sc.Logger.Info("Successfully write point to db", zap.Uint8("score", score), zap.String("sentence", sentence))
}
