package analysis

import (
	"github.com/cdipaolo/sentiment"
	"go.uber.org/zap"
)

func New(logger *zap.Logger) (sentiment.Models, error) {
	model, err := sentiment.Restore() //using pre-trained model
	if err != nil {
		logger.Warn("cannot init model", zap.Error(err))
		return nil, err
	}

	return model, nil
}
