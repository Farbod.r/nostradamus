# Nostradamus



### Nostradamus is the analytical service which preprocess data gather information and visualize them

### Serving http

Nostradamus requires [Golang](https://golang.org/) v1.17+ to build.

clone repo
```sh
$ cd nostradamus
$ go get
$ docker-compose up -d
$ go build
$ ./nostradamus serve
```



### How to run tests
TBD




## Technical Todos
- [ ] Expose metric route and scrap it
- [x] Increase test coverage
- [ ] Implement tracing
- [ ] Implement resource utilization monitoring
- [ ] Add docs about architecture
- [ ] Add Pipeline (ci/cd)
- [ ] Add Automated deployment
- [ ]