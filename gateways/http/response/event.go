package response

type EventResponse struct {
	Status string `json:"status"`
	Type   string `json:"type"`
	Data   Data   `json:"data"`
}
type Data struct {
	Challenge string `json:"challenge"`
}
