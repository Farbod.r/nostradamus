package handler

import (
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
	"net/http"
	"net/http/httptest"
	"nostradamus/internal/config"
	"testing"
)

func TestSayHi(t *testing.T) {
	//Arrange

	conf := config.Config{
		Env:   "prod",
		Debug: false,
	}

	handler := &EventHandler{
		Config: conf,
		Logger: zaptest.NewLogger(t),
	}

	req := httptest.NewRequest(echo.GET, "/", nil)
	rec := httptest.NewRecorder()
	e := echo.New()
	c := e.NewContext(req, rec)

	//Act and Assert
	if assert.NoError(t, handler.SayHi(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, "How you doin", rec.Body.String())
	}

}
