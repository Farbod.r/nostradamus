package handler

import (
	"net/http"
	"net/http/httptest"
	httpPkg "nostradamus/gateways/http"
	"nostradamus/internal/config"
	"strings"
	"testing"

	"github.com/kinbiko/jsonassert"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
)

func TestNewEvent(t *testing.T) {
	t.Parallel()

	testLogger := zaptest.NewLogger(t)

	conf := config.Config{
		Env:   "prod",
		Debug: false,
	}

	handler := &EventHandler{
		Config: conf,
		Logger: testLogger,
	}

	t.Run("empty body returns 400", func(t *testing.T) {
		t.Parallel()

		w := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/v1/tribe/event", nil)
		engine := echo.New()
		engine.POST("/v1/tribe/event", handler.NewEvent)
		engine.ServeHTTP(w, req)

		assert.Equal(t, http.StatusBadRequest, w.Code)
	})

	t.Run("webhook verification middleware test", func(t *testing.T) {
		t.Parallel()

		mw := httpPkg.Middleware{
			Config: conf,
			Logger: testLogger,
		}

		w := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/v1/tribe/event", nil)
		engine := echo.New()
		engine.POST("/v1/tribe/event", handler.NewEvent, mw.TribeWebhookVerification)
		engine.ServeHTTP(w, req)

		assert.Equal(t, http.StatusUnauthorized, w.Code)
	})

	t.Run("apikey limiter middleware test", func(t *testing.T) {
		t.Parallel()

		conf.Tribe.SigningSecret = "signsecrettest"
		mw := httpPkg.Middleware{
			Config: conf,
			Logger: testLogger,
		}

		w := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/v1/tribe/event", nil)
		engine := echo.New()
		engine.POST("/v1/tribe/event", handler.NewEvent, mw.APIKeyLimiter)
		engine.ServeHTTP(w, req)

		assert.Equal(t, http.StatusUnauthorized, w.Code)
	})

	t.Run("test tribe test-request", func(t *testing.T) {
		t.Parallel()

		w := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/v1/tribe/event", strings.NewReader(`{"networkId": "CAxOmI7I7X","context": "NETWORK","currentSettings": [],"type": "TEST","data": { "challenge":"d77dae12ea612cf647d6e2e78dd2b7470e4e4358"}}`))
		req.Header.Add("Content-Type", "application/json")
		engine := echo.New()
		engine.POST("/v1/tribe/event", handler.NewEvent)
		engine.ServeHTTP(w, req)

		assert.Equal(t, http.StatusOK, w.Code)
		ja := jsonassert.New(t)
		ja.Assertf(string(w.Body.Bytes()), `{"status": "SUCCEEDED", "type": "TEST",	"data": {"challenge": "d77dae12ea612cf647d6e2e78dd2b7470e4e4358"}}`)

	})

}
