package handler

import (
	"net/http"

	httpPkg "nostradamus/gateways/http"
	"nostradamus/gateways/http/request"
	"nostradamus/gateways/http/response"
	"nostradamus/internal/config"
	"nostradamus/internal/db/store"

	"github.com/cdipaolo/sentiment"
	"github.com/grokify/html-strip-tags-go"
	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
)

// names used in logger.
const (
	newEvent    = "http.event.newEvent"
	englishLang = sentiment.English
)

type EventHandler struct {
	Config         config.Config
	Store          store.Store
	Logger         *zap.Logger
	SentimentModel sentiment.Models
}

// Register routes and their middlewares
func (ev EventHandler) Register(echo *echo.Echo) {
	mw := httpPkg.Middleware{
		Config: ev.Config,
		Logger: ev.Logger,
	}

	echo.POST("/v1/tribe/event", ev.NewEvent, mw.TribeWebhookVerification, mw.TribeOldRequestsLimiter)

	echo.GET("/", ev.SayHi)

}
func (ev EventHandler) SayHi(c echo.Context) error {
	return c.String(http.StatusOK, "How you doin")
}

//nolint:funlen
func (ev EventHandler) NewEvent(c echo.Context) error {

	ev.Logger.Info("request", zap.String("type", newEvent))

	evReq := new(request.NewEventRequest)

	if err := c.Bind(&evReq); err != nil {

		ev.Logger.Warn("failed to read request body", zap.Error(err), zap.String("type", newEvent))

		return echo.ErrBadRequest
	}

	if err := evReq.Validate(); err != nil {

		ev.Logger.Warn("invalid request", zap.Error(err), zap.String("type", newEvent))

		return echo.ErrBadRequest
	}

	ev.Logger.Info("new validated request", zap.String("context", evReq.Context), zap.String("type", newEvent))

	switch evReq.EventTypeFromRequest() {

	case request.TEST:
		return ev.handleTestRequest(c, evReq)

	case request.SUBSCRIPTION:
		return ev.handleSubscriptionRequest(c, evReq)

	default:
		return ev.handleUnknownRequest(c, evReq)
	}

}

func (ev EventHandler) handleTestRequest(c echo.Context, evReq *request.NewEventRequest) error {
	ev.Logger.Info("request event test handled successfully", zap.Int("status", http.StatusOK), zap.String("type", newEvent))
	return c.JSON(
		http.StatusOK, response.EventResponse{
			Status: "SUCCEEDED",
			Type:   "TEST",
			Data:   response.Data{Challenge: evReq.Data.Challenge},
		})
}
func (ev EventHandler) handleUnknownRequest(c echo.Context, evReq *request.NewEventRequest) error {
	ev.Logger.Info("request event known handled", zap.Int("status", http.StatusBadRequest), zap.String("type", newEvent))
	return c.JSON(
		http.StatusBadRequest, response.EventResponse{
			Status: "FAILED",
		})
}
func (ev EventHandler) handleSubscriptionRequest(c echo.Context, evReq *request.NewEventRequest) error {
	sentence := strip.StripTags(evReq.Data.Object.ShortContent)
	analysis := ev.SentimentModel.SentimentAnalysis(sentence, englishLang)

	go ev.Store.SentenceCollection.Insert(analysis.Score, sentence, evReq.Data.Time, map[string]string{
		"role":      evReq.Data.Actor.RoleType,
		"networkId": evReq.NetworkId,
		"objectId":  evReq.Data.Object.Id,
	})

	//if ev.Config.App.WordAnalysisEnabled {
	//	go ev.Store.WordCollection.Insert()
	//}

	ev.Logger.Info("request event test handled successfully", zap.Int("status", http.StatusOK), zap.String("type", newEvent))
	return c.String(http.StatusOK, "Success")
}
