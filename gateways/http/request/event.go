package request

import (
	"fmt"
	"time"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type NewEventRequest struct {
	NetworkId string    `json:"networkId"`
	Context   string    `json:"context"`
	Type      string    `json:"type"`
	Data      EventData `json:"data"`
}
type EventData struct {
	Challenge string     `json:"challenge"`
	Time      time.Time  `json:"time"`
	Actor     ActorData  `json:"actor"`
	Object    ObjectData `json:"object"`
}
type ActorData struct {
	Id       string `json:"id"`
	RoleType string `json:"roleType"`
}
type ObjectData struct {
	CreatedAt    time.Time `json:"createdAt"`
	ShortContent string    `json:"shortContent"`
	Id           string    `json:"id"`
}

// Validate method for newEvent Request
func (ev NewEventRequest) Validate() error {
	if err := validation.ValidateStruct(&ev,
		// NetworkID should be a required, non-empty string
		validation.Field(&ev.NetworkId, validation.Required, validation.Length(1, 0)),
		// Type is required
		validation.Field(&ev.Type, validation.Required, validation.In("TEST", "SUBSCRIPTION")),
		// Check the MessageContent validate method
		validation.Field(&ev.Data, validation.Required),
	); err != nil {
		return fmt.Errorf("message request validation failed: %w", err)
	}

	return nil
}

type EventType int

const (
	TEST         EventType = 1
	SUBSCRIPTION EventType = 2
	UNKNOWN      EventType = 3
)

func (ev NewEventRequest) EventTypeFromRequest() EventType {
	switch ev.Type {
	case "TEST":
		return TEST
	case "SUBSCRIPTION":
		return SUBSCRIPTION
	default:
		return UNKNOWN
	}
}
