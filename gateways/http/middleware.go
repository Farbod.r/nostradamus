package http

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"nostradamus/internal/config"

	"github.com/labstack/echo/v4"
	"go.uber.org/zap"
)

const oldRequestsThreshold = time.Minute * 15

// Middleware is the binding struct for custom fiber middlewares.
type Middleware struct {
	Config config.Config
	Logger *zap.Logger
}

// APIKeyLimiter Blocks requests without proper header
func (mw Middleware) APIKeyLimiter(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		apiKey := context.Request().Header.Get("X-Tribe-Signature")
		if strings.Compare(apiKey, mw.Config.Tribe.SigningSecret) != 0 {
			mw.Logger.Warn("Unauthorized access: ", zap.String("IP", context.RealIP()))
			return echo.NewHTTPError(http.StatusUnauthorized)
		}

		return next(context)
	}
}

// TribeWebhookVerification Blocks requests without proper signature
func (mw Middleware) TribeWebhookVerification(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {
		timeStamp := context.Request().Header.Get("X-Tribe-Request-Timestamp")
		reqBody, err := ioutil.ReadAll(context.Request().Body)

		err = context.Request().Body.Close()
		if err != nil {
			mw.Logger.Warn("Cannot close req body", zap.Error(err))
			return echo.NewHTTPError(http.StatusBadRequest)
		}
		context.Request().Body = ioutil.NopCloser(bytes.NewBuffer(reqBody))

		if err != nil {
			mw.Logger.Warn("Cannot get req body", zap.Error(err))
			return echo.NewHTTPError(http.StatusBadRequest)
		}

		baseString := getBaseString(timeStamp, string(reqBody))

		tribeHmac := hmac.New(sha256.New, []byte(mw.Config.Tribe.SigningSecret))
		tribeHmac.Write([]byte(baseString))

		if strings.Compare(context.Request().Header.Get("X-Tribe-Signature"), hex.EncodeToString(tribeHmac.Sum(nil))) != 0 {
			mw.Logger.Warn("Unauthorized access: ", zap.String("IP", context.RealIP()))
			return echo.NewHTTPError(http.StatusUnauthorized)
		}

		return next(context)
	}
}

// TribeOldRequestsLimiter Blocks old requests
func (mw Middleware) TribeOldRequestsLimiter(next echo.HandlerFunc) echo.HandlerFunc {
	return func(context echo.Context) error {

		unixTime, err := strconv.ParseInt(context.Request().Header.Get("X-Tribe-Request-Timestamp"), 10, 64)
		if err != nil {
			mw.Logger.Warn("Cannot get time from request", zap.Error(err))
			return echo.NewHTTPError(http.StatusBadRequest)
		}

		requestTime := time.Unix(unixTime, 0)
		currentTime := time.Now()

		if requestTime.Add(oldRequestsThreshold).Before(currentTime) {
			mw.Logger.Warn("Old request received", zap.Time("request time", requestTime))
			return echo.NewHTTPError(http.StatusBadRequest)
		}

		return next(context)
	}
}

func getBaseString(timeStamp string, rawBody string) string {
	return timeStamp + ":" + rawBody
}
