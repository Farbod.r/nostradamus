package cmd

import (
	"net/http"
	"nostradamus/internal/analysis"
	"nostradamus/internal/db/store"

	"nostradamus/gateways/http/handler"
	"nostradamus/internal/config"
	"nostradamus/internal/db/influx"
	nostradamusLog "nostradamus/internal/logger"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/cobra"
	"github.com/tylerb/graceful"
	"go.uber.org/zap"
)

func addServeCMD(rootCMD *cobra.Command) {
	serveCmd := &cobra.Command{
		Use:   "serve",
		Short: "serve command runs nostradamus server",
		Run: func(cmd *cobra.Command, args []string) {
			initServices()
		},
	}
	rootCMD.AddCommand(serveCmd)
}

func initServices() {
	//init config and logger
	conf := config.New(".")
	logger := nostradamusLog.New(conf)

	logger.Info("loaded config and logger: ", zap.Any("cfg", conf))

	//init echo webserver
	e := initEcho(logger)

	//init db
	dbClient, err := influx.Connect(conf, logger)
	defer dbClient.Close()
	if err != nil {
		logger.Panic("db failed")
	}
	dbStore := store.New(conf, &dbClient, logger)

	//init model
	model, err := analysis.New(logger)
	if err != nil {
		logger.Panic("model failed")
	}

	logger.Info("done with creating dependencies")

	eventHandler := handler.EventHandler{
		Config:         conf,
		Logger:         logger,
		Store:          *dbStore,
		SentimentModel: model,
	}
	eventHandler.Register(e)

	//also serve on 1234 for profiling
	go func() {
		err := http.ListenAndServe(":1234", nil)
		if err != nil {
			logger.Fatal("error serving on :1234", zap.Error(err))
		}
	}()

	logger.Fatal(graceful.ListenAndServe(e.Server, conf.App.GracefulTimeout).Error())
}

func initEcho(logger *zap.Logger) *echo.Echo {
	e := echo.New()
	e.Use(middleware.Recover())

	// for sake of manual test
	if config.Conf.Debug {
		e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
			Format: "time=${time_rfc3339}, " +
				"X-Tribe-Signature=${header:X-Tribe-Signature}, " +
				"method=${method}, " +
				"uri=${uri}, " +
				"latency=${latency_human}, " +
				"status=${status}\n",
		}))

		e.Use(middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {
			if c.Request().RequestURI == "/metrics" {
				return // do not dump metrics
			}

			logger.Info(string(reqBody))
			logger.Info(string(resBody))
		}))
	}
	//

	//web.RegisterRoutes(e)
	e.Server.Addr = ":" + config.Conf.App.Port

	return e
}
