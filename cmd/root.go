package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

func Execute() {
	var rootCMD = &cobra.Command{
		Use:   "nostradamus",
		Short: "nostradamus : the true predictor", //TODO: Add autocomplete
	}

	addInfoCMD(rootCMD)
	addServeCMD(rootCMD)

	if err := rootCMD.Execute(); err != nil {
		println("ERROR:", err.Error())
		os.Exit(1)
	}
}
