package cmd

import (
	"fmt"

	"nostradamus/internal/config"

	"github.com/spf13/cobra"
)

func addInfoCMD(rootCMD *cobra.Command) {
	versionCmd := &cobra.Command{
		Use:   "info",
		Short: "load and print service information",
		Run: func(cmd *cobra.Command, args []string) {
			showInfo()
		},
	}

	rootCMD.AddCommand(versionCmd)
}

func showInfo() {
	config.New(".")
	fmt.Printf("%+v\n", config.Conf)

	fmt.Println()
}
